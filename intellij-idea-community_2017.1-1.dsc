-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: intellij-idea-community
Binary: intellij-idea-community
Architecture: all
Version: 2017.1-1
Maintainer: Marcel Michael Kapfer <marcelmichaelkapfer@yahoo.co.nz>
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 7.0.50~)
Package-List:
 intellij-idea-community deb devel optional arch=all
Checksums-Sha1:
 2af60d7bb0d2765946394c4200dac6e38abe4b29 5641 intellij-idea-community_2017.1.orig.tar.gz
 2809a8418e7d68a2389c45c1d6201f51a8a380a1 2624 intellij-idea-community_2017.1-1.debian.tar.xz
Checksums-Sha256:
 93646f0ed33fb1c62473e12c18f9fa7d90ade1ec3ec2c00b92eafd9fac728882 5641 intellij-idea-community_2017.1.orig.tar.gz
 e9fb258e42f8ff318ef311b78f5f45a2a27314cf7f08cdd90c668ce0ccbf7c63 2624 intellij-idea-community_2017.1-1.debian.tar.xz
Files:
 38a2df9af92629b31beaa46222d3e00e 5641 intellij-idea-community_2017.1.orig.tar.gz
 df588829e9b6592ca145d054a07ec85a 2624 intellij-idea-community_2017.1-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIcBAEBCAAGBQJY1SYCAAoJECuF2UqwCcGYijsP/1VJ5aC4hwiPV6sH8/eRdROT
eNqsrTiL+SXvetrQwBj+8/JbeZOx44J7Yxfk2bRmoodBOCbr+JkGe9dedKs6NHuR
Fp6H5jbdmgcE6SdHTp2Juic+947bmGcQcBvnCTXWoEBSDvTWcUjRnaRQoJQ06p0y
+BA9lLZrskh1LcvRBbu60bwif7RpAHrxtGer6Vc3e+XUWUDHbzifxLS4lEtm1IDp
1aiZ1NzO1N7bD3w0OEWPcgDfGrTQRHJazclXL58EdtIkgMcCwls4kqd5fe+ExPMR
9BW3dAd9JydJS2va6FmmFuwc6f6iWaOXgGYAi9iaG0WGpH6lIxpIfKVRzzH1O85U
LEi9CtGi+dZb3hYicHvecY47+XscfRwkILiiqHaVnXbbLJ2uXg1G3qjYu0KToLXd
0/z9noPHiBWRt4Optcj7xNjcUZBHrRrSiuFMtqp5xlk51ElLyk0P3AHBoLWQ7l+e
yD2XbD1KN0im/WPXlfzrlbm752mUrPMT0FYTdDX3A48PQ0dPMApq10ApoNpNkuzF
K4hkv9uFXj8iG6W21bLjSXMMzuYiPuk99hpB6tQ+22iA6dHE9og1UZbcwsv8p4s3
+ZPr6DaX3+V3oJp7jYiLfzRxbwo0ZC67QFkYvv0As5TJi3ncW/SqlkZLY1WNZXMp
D4U5Y43wG8QtVx5AhuOq
=Os6e
-----END PGP SIGNATURE-----
